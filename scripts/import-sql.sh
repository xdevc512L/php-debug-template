#!/bin/bash
set -o allexport
# shellcheck source=/dev/null
source .env
set +o allexport

podman exec -i configurador-leroy_mariadb mysql -u"${MYSQL_USER}" -p"${MYSQL_PASSWORD}" "${MYSQL_DATABASE}" < data.sql
